package com.tutorial.dynamocrud.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.tutorial.dynamocrud.dto.UsuarioDto;
import com.tutorial.dynamocrud.entity.Usuario;
import com.tutorial.dynamocrud.service.UsuarioService;

@RestController
public class UsuarioController {

	UsuarioService usuarioService;

	public UsuarioController(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	@GetMapping("/usuario")
	public ResponseEntity<Iterable<Usuario>> list() {
		return ResponseEntity.ok(usuarioService.lista());
	}

	@GetMapping("/usuario/{usuarioId}")
	public ResponseEntity<?> getOne(@PathVariable("usuarioId") String usuarioId) {
		if (!usuarioService.existsId(usuarioId)) {
			return new ResponseEntity<>("no existe", HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok(usuarioService.getOne(usuarioId));
	}

	@PostMapping("/usuario")
	public ResponseEntity<?> create(@RequestBody UsuarioDto usuarioDto) {
		if (usuarioService.existsId(usuarioDto.getId())) {
			return new ResponseEntity<>("el id ya existe", HttpStatus.BAD_REQUEST);
		}
		if (usuarioService.existsNombre(usuarioDto.getNombre())) {
			return new ResponseEntity<>("ese nombre ya existe", HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok(usuarioService.save(usuarioDto));
	}

	@PutMapping("/usuario")
	public ResponseEntity<?> update(@RequestBody UsuarioDto usuarioDto) {
		if (!usuarioService.existsId(usuarioDto.getId())) {
			return new ResponseEntity<>("no existe", HttpStatus.NOT_FOUND);
		}
		if (usuarioService.existsNombre(usuarioDto.getNombre())
				&& !usuarioService.getOne(usuarioDto.getId()).getNombre().equals(usuarioDto.getNombre())) {
			return new ResponseEntity<>("ese nombre ya existe", HttpStatus.BAD_REQUEST);
		}
		return ResponseEntity.ok(usuarioService.update(usuarioDto));
	}

	@DeleteMapping("/usuario/{usuarioId}")
	public ResponseEntity<?> delete(@PathVariable("usuarioId") String usuarioId) {
		if (!usuarioService.existsId(usuarioId)) {
			return new ResponseEntity<>("no existe", HttpStatus.NOT_FOUND);
		}
		usuarioService.delete(usuarioId);
		return new ResponseEntity<>("usuario eliminado", HttpStatus.OK);
	}

}
