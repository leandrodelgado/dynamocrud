package com.tutorial.dynamocrud.repository;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.tutorial.dynamocrud.entity.Usuario;

@EnableScan
@Repository
public interface UsuarioRepository extends CrudRepository<Usuario, String> {

	boolean existsByNombre(String nombre);
}
